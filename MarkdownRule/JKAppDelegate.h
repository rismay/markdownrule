//
//  JKAppDelegate.h
//  MarkdownRule
//
//  Created by Joris Kluivers on 5/22/12.
//  Copyright (c) 2012 Cardcloud. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@interface JKAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet WebView *webview;

@end
