//
//  main.m
//  MarkdownRule
//
//  Created by Joris Kluivers on 5/22/12.
//  Copyright (c) 2012 Cardcloud. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
